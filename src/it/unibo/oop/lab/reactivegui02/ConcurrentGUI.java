package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.*;

public class ConcurrentGUI extends JFrame {

    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;

    /**
     * 
     */
    private static final long serialVersionUID = -8630968055862320453L;

    final JLabel display = new JLabel("display");
    final JButton up = new JButton("up");
    final JButton down = new JButton("down");
    final JButton stopbutton = new JButton("stop");
    
    public ConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stopbutton);
        this.getContentPane().add(panel);
        this.setVisible(true);
        // dafkjdfdkhfkdhsfjkdshk
        final Agent agent = new Agent();
        new Thread(agent).start();
        /*
         * Register a listener that stops it
         */
        stopbutton.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button stop.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            @Override
            public void actionPerformed(final ActionEvent e) {
                // Agent should be final
                agent.stopCounting();
            }
        });
        up.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                agent.setUp();
            }
        });
        down.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                agent.setDown();
            }
        });
    }

    /*
     * The counter agent is implemented as a nested class. This makes it
     * invisible outside and encapsulated.
     */
    private class Agent implements Runnable {
        /*
         * stop is volatile to ensure ordered access
         */
        private volatile boolean stop;
        private volatile boolean isUP = true;
        private int counter;

        public void run() {
            while (!this.stop) {
                try {
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
//                    if(this.isUP) {
//                        this.counter++;
//                    } else {
//                        this.counter--;
//                    }
                    counter += isUP ? 1 : -1;
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            ConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    /*
                     * This is just a stack trace print, in a real program there
                     * should be some logging and decent error reporting
                     */
                    throw new IllegalArgumentException();
                }
            }
        }

        /**
         * External command to stop counting.
         */
        public void stopCounting() {
            this.stop = true;
            SwingUtilities.invokeLater(() -> {
                up.setEnabled(false);
                down.setEnabled(false);
                ConcurrentGUI.this.stopbutton.setEnabled(false);
            });
        }
        
        public void setUp() {
            this.isUP = true;
        }
        
        public void setDown() {
            this.isUP = false;
        }
    }
}
